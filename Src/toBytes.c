#include <stdio.h>
#include "../Include/toBytes.h"

void bytesInt(int valor){
	unsigned char *p;
	int i; 
	p = (unsigned char *) &valor;
	for(i=0;i<sizeof(valor);i++){
		printf("Byte %d:    %x\n",i,(int)*p);
		p++;	
	}

}

void bytesLong(long valor){
	unsigned char *p;
	int i;
	p = (unsigned char *) &valor;
	for(i=0;i<sizeof(valor);i++){
		printf("Byte %d:    %lx\n",i,(long)*p);
		p++;	
	}
}


void bytesFloat(float valor){
	unsigned char *p;
	int i;
	p = (unsigned char *) &valor;
	for(i=0;i<sizeof(valor);i++){
		printf("Byte %d:    %x\n",i,(int)*p);
		p++;	
	}
}

void bytesDouble(double valor){
	unsigned char *p;
	int i;
	p = (unsigned char *) &valor;
	for(i=0;i<sizeof(valor);i++){
		printf("Byte %d:    %lx\n",i,(long)*p);
		p++;	
	}
}

/*int main(){
	
	bytesInt(1200);
	//bytesLong(1200);
//	bytesFloat(53);		
//	bytesDouble(1200);		
return 0;
} */
