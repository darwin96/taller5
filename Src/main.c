#include<stdio.h>
#include "../Include/toBytes.h"
#include "../Include/datosUrl.h"
#include "../Include/ocurrencias.h"
#include "../Include/imc.h"
#include "../Include/iva.h"
#include "../Include/direcciones.h"


int main(){
	printf("+++++ejercicio 1++++++\n");
	printf("Dato entrante: 1200\n");
	bytesInt(1200);
	printf("\n\nDato entrante: 1222\n");
	bytesInt(1222);


	printf("\n\n+++++ejercicio2+++++");
	printf("URL: hptt://www.fiec.espol.edu.ec\n");
	datosUrl("hptt://www.fiec.espol.edu.ec");

	printf("\n\n++++++ejercicio3++++++");
	printf("int arreglo[]={2,2,3,4,5}     ocurrencias(arreglo,5,3)\n");
	int arreglo[]={2,2,3,4,5};
	ocurrencias(arreglo,5,3);
	
	printf("\n\n++++++ejercicio3+++++");
	printf("Peso: %i\n Altura%i\n",53,163);
	float *p=NULL,total=0;
	float **resultado = NULL;
	p=&total;
	resultado=&p;
	imc(53,163,resultado);
	printf("Su indice de masa corporal es: %f\n",**resultado);
	
	printf("+++++++ejercicio5++++++++\n\n");
	Producto producto = {
		"laFavorita",
		20.0,
		"hola",
		0
	};
	Producto *q;
	q = &producto; 
	iva(q);
        printf("%f\n",q->precioIVA);
	
	printf("\n\n+++++ejercicio6++++++\n");
	
	int a[3]={3,4,5};
	struct Producto *p1=(struct Producto*)malloc(sizeof(struct Producto*));
	struct Producto *p2=(struct Producto*)malloc(sizeof(struct Producto*));
	struct Producto *p3=(struct Producto*)malloc(sizeof(struct Producto*));
	struct Producto *p4=(struct Producto*)malloc(sizeof(struct Producto*));
	struct Producto *p5=(struct Producto*)malloc(sizeof(struct Producto*));
	struct Producto *p6=(struct Producto*)malloc(sizeof(struct Producto*));
	
	struct Producto *arr_productos[6]={p1,p2,p3,p4,p5,p6};
	direcciones(arr_productos,a);


return 0;
}
