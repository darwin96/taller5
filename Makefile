#COLOCAR EN EL TERMINAL make Y DESPUES ./Bin/programa :)
all: programa
CC=gcc -Wall -c
programa: main.o toBytes.o datosUrl.o ocurrencias.o imc.o iva.o direcciones.o
	gcc Obj/main.o Obj/toBytes.o Obj/datosUrl.o Obj/ocurrencias.o Obj/imc.o Obj/iva.o Obj/direcciones.o -o Bin/programa
direcciones.o: Src/direcciones.c
	$(CC) Src/direcciones.c -o Obj/direcciones.o
iva.o: Src/iva.c
	$(CC) Src/iva.c -o Obj/iva.o
imc.o: Src/imc.c
	$(CC) Src/imc.c -o Obj/imc.o

datosUrl.o: Src/datosUrl.c
	$(CC) Src/datosUrl.c -o Obj/datosUrl.o
ocurrencias.o: Src/ocurrencias.c
	$(CC) Src/ocurrencias.c -o Obj/ocurrencias.o
main.o: Src/main.c
	$(CC) Src/main.c -o Obj/main.o
toBytes.o: Src/toBytes.c
	$(CC) Src/toBytes.c -o Obj/toBytes.o
.PHONY: clean
clean: 
	rm Bin/* Obj/* 
